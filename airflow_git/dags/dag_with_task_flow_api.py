from datetime import datetime, timedelta

from airflow.decorators import dag, task

default_args = {
    'owner': 'Vicky',
    'retries': 5,
    'retry_delay': timedelta(minutes=5)
}

@dag(
    dag_id='dag_with_task_flow_api',
    default_args=default_args,
    start_date=datetime(2022, 9, 3),
    schedule_interval='@daily'
)
def hello_world_et():

    @task()
    def get_name():
        return "Jerry"

    @task()
    def get_age():
        return 19

    @task()
    def greet(name, age):
        print(f"Hello World! My name is {name} and I am {age} years old.")

    name = get_name()
    age = get_age()
    greet(name=name, age=age)

greet_dag = hello_world_et()
