from  datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator

default_args = {
    'owner': 'Vicky',
    'retries': 5,
    'retry_delay': timedelta(minutes=2)
}


with DAG(
    dag_id='our_first_dag',
    default_args=default_args,
    description='This is my first dag',
    start_date=datetime(2022, 9, 1, 14, 40),
    schedule_interval='@daily'
) as dag:
    task1 = BashOperator(
        task_id='first_task',
        bash_command="echo Hello world"
    )
    task2 = BashOperator(
        task_id='second_task',
        bash_command="echo  I am second task"
    )
    task3 = BashOperator(
        task_id='third_task',
        bash_command="echo I am third task"
    )
    # task1.set_downstream(task2) # Basically task2 execute after task1
    # task1.set_downstream(task3) # Basically task3 execute after task1
    # task1 >> task2  # means task2 will be execute after task1
    # task1 >> task3  # means task3 will be execute after task1

    task1 >> [task2, task3]  # another method to initialize the task

